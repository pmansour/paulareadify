﻿using System;
using System.ServiceModel;

namespace RedServicePaula
{
    public class RedPill : IRedPill
    {
        public ReadifyPuzzles.TrianglePuzzle.TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            try
            {
                var puzzel = new ReadifyPuzzles.TrianglePuzzle.Puzzle();
                return puzzel.GetTriangleType(a, b, c);
            }
            catch (Exception ex)
            {
                // Log Exception
                throw ex;
            }
        }

        public System.Guid WhatIsYourToken()
        {
            // Hardcode my Token :)
            return new Guid("8ad9c6e0-9544-4224-8712-1c1760068eaa");
        }

        public string ReverseWords(string s)
        {
            try
            {
                var puzzel = new ReadifyPuzzles.ReverseWordsPuzzle.Puzzle();
                return puzzel.ReverseString(s);
            }
            catch (Exception ex)
            {
                // Log Exception
                throw new FaultException<Exception>(ex, ex.Message);
            }
        }

        public long FibonacciNumber(long n)
        {
            try
            {
                var puzzel = new ReadifyPuzzles.FibonacciNumberPuzzle.Puzzle();
                return puzzel.FibonacciNumber(n);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Log Exception
                throw new FaultException<ArgumentOutOfRangeException>(ex, ex.Message);
            }
            catch (Exception ex)
            {
                // Log Exception
                throw new FaultException<Exception>(ex, ex.Message);
            }
        }
    }
}
