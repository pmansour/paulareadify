﻿using System;
using System.ServiceModel;
using ReadifyPuzzles.TrianglePuzzle;

namespace RedServicePaula
{
    [ServiceContract(Namespace = "http://KnockKnock.readify.net")]
    public interface IRedPill
    {
        [OperationContract]
        TriangleType WhatShapeIsThis(int a, int b, int c);

        [OperationContract]
        Guid WhatIsYourToken();

        [OperationContract]
        string ReverseWords(string s);

        [OperationContract]
        long FibonacciNumber(long n);
    }
}
