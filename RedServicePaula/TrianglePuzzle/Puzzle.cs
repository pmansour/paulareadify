﻿
namespace ReadifyPuzzles.TrianglePuzzle
{
    /// <summary>
    /// Get the type of the triangle
    /// </summary>
    public class Puzzle
    {
        /// <summary>
        /// Write a function that receives three integer inputs for the lengths of the sides of a triangle 
        /// and returns one of four values to determine the triangle type.
        /// 1.	Scalene
        /// 2.	Isosceles
        /// 3.	Equilateral
        /// 4.	Error
        /// </summary>
        /// <param name="a">the length of the 1st side</param>
        /// <param name="b">the length of the 2nd side</param>
        /// <param name="c">the length of the 3rd side</param>
        /// <returns></returns>
        public TriangleType GetTriangleType(decimal a, decimal b, decimal c)
        {
            TriangleType triangleType = TriangleType.Error;

            if (IsValidSides(a, b, c))
            {
                if (a == b && a == c) // check if all sides are equal
                {
                    triangleType = TriangleType.Equilateral;
                }
                else if (a == b || a == c || b == c) // check if any two sides are equal
                {
                    triangleType = TriangleType.Isosceles;
                }
                else // otherwise all sides are not equal
                {
                    triangleType = TriangleType.Scalene;
                }
            }

            return triangleType;
        }

        /// <summary>
        /// return true in case of all sides are positive and the sum of any two side should be more than the third side
        /// otherwise return false
        /// </summary>
        /// <param name="a">the length of the 1st side</param>
        /// <param name="b">the length of the 2nd side</param>
        /// <param name="c">the length of the 3rd side</param>
        /// <returns></returns>
        private bool IsValidSides(decimal a, decimal b, decimal c)
        {
            if (a > 0 && b > 0 && c > 0) // check if the length of all trianagle sides are positive number
            {
                if (a + b > c && a + c > b && b + c > a) // the sum of any two side should be more than the third side otherwise error
                {
                    return true;
                }
            }
            return false;
        }

    }
}
