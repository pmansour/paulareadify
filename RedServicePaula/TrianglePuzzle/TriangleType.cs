﻿
namespace ReadifyPuzzles.TrianglePuzzle
{

    /// <summary>
    /// Enum contains the triangle types or not valid type
    /// 1.	Scalene
    /// 2.	Isosceles
    /// 3.	Equilateral
    /// 4.  NotValid
    /// </summary>
    public enum TriangleType
    {
        /// <summary>
        /// The tree sides are not valid to make a triangle
        /// </summary>
        Error = 0,

        /// <summary>
        /// Three equal sides
        /// </summary>
        Equilateral = 1,

        /// <summary>
        /// Two equal sides
        /// </summary>
        Isosceles = 2,

        /// <summary>
        /// No equal sides
        /// </summary>
        Scalene = 3
    }

}
