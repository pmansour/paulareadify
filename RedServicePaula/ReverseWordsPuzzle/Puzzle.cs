﻿
namespace ReadifyPuzzles.ReverseWordsPuzzle
{
    /// <summary>
    /// Write a function to reverse the words in a string, 
    /// for example “cat and dog” becomes “tac dna god”.
    /// </summary>
    public class Puzzle
    {
        /// <summary>
        /// Reverse the words in the entire string
        /// • Should reverse each word within the string without altering whitespace characters
        /// • Should treat punctuation characters as part of the word
        /// </summary>
        /// <param name="str">string to reverse</param>
        /// <returns></returns>
        public string ReverseString(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;

            char[] strArr = str.ToCharArray();

            int i = 0, j = 0;
            while (i <= strArr.Length)
            {
                if (i == strArr.Length || strArr[i] == ' ') // check it's the last word or the current char is space
                {
                    ReverseString(ref strArr, j, i - 1); // reverse the current word
                    j = i + 1;
                }
                i++;
            }
            return new string(strArr); // return the string array after reversing it
        }

        /// <summary>
        /// Reverse the characters of the entire array from index "from" to index "to"
        /// </summary>
        /// <param name="strArr">entire array of char to reverse</param>
        /// <param name="from">start reverse from index</param>
        /// <param name="to">end reverse to index</param>
        private void ReverseString(ref char[] strArr, int from, int to)
        {
            int mid = (to - from) / 2; // get the middle to loop
            for (int i = 0; i <= mid; i++) // swap the chars in the enetire array from/to
            {
                char temp = strArr[to - i];
                strArr[to - i] = strArr[from + i];
                strArr[from + i] = temp;
            }
        }
    }
}
