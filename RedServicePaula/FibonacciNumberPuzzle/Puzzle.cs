﻿using System;
using RedServicePaula.Resources;

namespace ReadifyPuzzles.FibonacciNumberPuzzle
{
    /// <summary>
    /// Get the type of the triangle
    /// </summary>
    public class Puzzle
    {
        private const int FibonacciNumberMaximum = 92;


        /// <summary>
        /// To Calculate Fibonacci Number based on the Equation F(n) = F(n-1) + F(n-2)
        /// and also use the negative equation F(n-2) = F(n) - F(n-1)
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public long FibonacciNumber(long n)
        {
            long FN = 0;
            long number = n;
            if (n < 0)
                number = -1 * n;
            if (number == 0)
            {
                FN = 0;
            }
            else if (number == 1)
            {
                FN = 1;
            }
            else if (number > FibonacciNumberMaximum)
            {
                throw new ArgumentOutOfRangeException(string.Format(ExceptionResource.FibonacciNumberOutOfRange, FibonacciNumberMaximum));
            }
            else //if (number > 1)
            {
                long tmp;
                long FN_2 = 0;
                long FN_1 = 1;
                FN = FN_1 + FN_2;
                for (int i = 2; i < number; i++)
                {
                    tmp = FN_1;
                    FN_1 = FN;
                    FN_2 = tmp;
                    FN = FN_1 + FN_2;
                }
            }
            if (n < 0 && number % 2 == 0)
                FN = -1 * FN;
            return FN;
        }

    }
}
